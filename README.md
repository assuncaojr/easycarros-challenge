## EasyCar Challenge

Guia de tecnologia, Instalação, uso e manutenção da front.

## Requisitos
Não há restrição de sistema operacional para a instalação do Front. Porém, recomendamos o uso de alguma distribuição linux.

Como dependência do sistema é necessário a instalação das ferramentas:
-  **[Nodejs](https://nodejs.org/en/)** versão maior ou igual ao 12.16.3.

## Tecnologias

- **[Typescript](https://www.typescriptlang.org/)** - Typescript
- **[React.JS](https://pt-br.reactjs.org/)** - Framework React.JS
- **[Ant.Design](https://ant.design/)** - Framerowk Visual/UI.


## Variáveis de ambiente
Para configurar as variáveis de ambiente cria uma cópia do arquivo  '.env.example' com nome '.env' na raiz do projeto. As variáveis utilizadas são:
```
REACT_APP_ENV=development   // ambiente de execução utilize 'development' para local e 'production' para produção.
REACT_APP_API_BASE_ENDPOINT= //entrypoint da API
REACT_APP_NAME= // Nome/Título do projeto

```

## Executando em ambiente de desenolvimento com `yarn`

  1. Entre na raiz do projeto
  2. Caso não tenha instalado as depedências do projeto, execute o comando `yarn`
  3. Execute o comando `yarn star`

Ao executar o comando acima, a aplicação estará disponivel em "http://localhost:3000"
