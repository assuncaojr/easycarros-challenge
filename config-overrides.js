const { override, fixBabelImports, addLessLoader } = require('customize-cra');

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      '@primary-color': '#05202A',
      '@layout-body-background': '#fff',
      '@layout-header-background': '#fff',
      '@layout-footer-background': '#4B4B4B'
    }
  })
);