import querystring from 'query-string'
import { BASE_ENDPOINT } from 'settings'
import { enUser } from 'enums'

export const hasSuccessStatus = (response: any) => response.status >= 200 && response.status < 300

export const getResponseErrorMessage = async (response: any) => {
  const body = await response.json()

  if (body) return body
  return 'Error'
}

const request = async (endpoint: string, requestOptions: any) => {
  if (requestOptions.query) endpoint += '?' + querystring.stringify(requestOptions.query)

  const options = {
    ...requestOptions,
    body: JSON.stringify(requestOptions.body),
    headers: {
      ...requestOptions.headers,
      'Content-Type': 'application/json',
    }
  }

  const response = await fetch(endpoint, options)

  if (hasSuccessStatus(response)) {
    return response.status === 204
      ? null
      : response.json()
  }

  throw await getResponseErrorMessage(response)
}

export const fetchApi = async (endpoint: string, requestOptions: any) => {
  const token = localStorage.getItem(enUser.TOKEN)

  const options = {
    ...requestOptions,
    headers: {
      Authorization: `Bearer ${token}`,
      ...requestOptions.headers
    }
  }

  return await request(`${BASE_ENDPOINT}/${endpoint}`, options)
}
