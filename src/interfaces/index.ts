export interface IError {
  stack: string
  message: string
}

export interface IUser {
  email: string
  password?: string
  token?: string
  tokenExp?: string
}

export interface IVehicle {
  id: string
  plate: string
}
