import React from 'react'
import Pages from 'components/Pages'

const App: React.FC = () => (
  <Pages />
)

export default App
