
export enum enRowJustify {
  start = 'start',
  end = 'end',
  center = 'center',
  spanceAround = 'space-around',
  spanceBetween = 'space-between'
}

export enum enBaseColors {
  PRIMARY = '#157E62',
  SECONDARY = '#673DC7',
  THIRD = '#894033',
  FOURTH = '#F8AB26'
}

export enum enUser {
  EMAIL = 'USER_ACCESS_EMAIL',
  TOKEN = 'USER_ACCESS_TOKEN',
  EXPIRE = 'USER_ACCESS_EPIRES'
}
