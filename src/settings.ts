export const ENV = process.env.REACT_APP_ENV
export const IS_DEVELOPMENT = ENV === 'development'
export const BASE_ENDPOINT = process.env.REACT_APP_API_BASE_ENDPOINT
export const SITE_NAME = process.env.REACT_APP_SITE_NAME
