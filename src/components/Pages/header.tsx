import React from 'react'
import logo from 'assets/car.png'
import { Layout, Typography } from 'antd'
import { SITE_NAME } from 'settings'
import Container from 'components/Shared/Container'

const Header = () => {
  const { Header } = Layout
  const mainStyle = { background: '#fff' }

  return (
    <div style={mainStyle}>
      <Container>
        <Header>
          <Typography.Title level={4} className="main-title">
            <img src={logo} className="logo" alt={SITE_NAME} />

            {SITE_NAME}
          </Typography.Title>
        </Header>
      </Container>
    </div>
  )
}

export default Header
