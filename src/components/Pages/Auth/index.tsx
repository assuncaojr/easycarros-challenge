import React, { useState } from 'react'
import { Layout, Row, Col, Input, Button } from 'antd'
import { fetchApi } from 'helpers/api'
import { handleError } from 'helpers/errors'
import { IUser } from 'interfaces'
import { enUser } from 'enums'
import Car from 'assets/car.png'
import jwtDecode from 'jwt-decode'
import { SITE_NAME } from 'settings'
import './style.less'

interface IProps {
  callback: Function
}

const Auth = ({callback}: IProps) => {
  const { Content } = Layout
  const [loading, setLoading] = useState(false)

  const [model, setModel] = useState<IUser>({
    email: '',
    password: ''
  })

  const handleAuth = async () => {
    try {
      setLoading(false)
      const { data } = await fetchApi('auth', {
        method: 'POST',
        body: model
      })

      const { token } = data
      const { exp } = jwtDecode(data.token)
      callback(token)

      localStorage.setItem(enUser.EMAIL, model.email)
      localStorage.setItem(enUser.TOKEN, token)
      localStorage.setItem(enUser.EXPIRE, exp)
    } catch (err) {
      const { error } = err
      handleError(error || err)
    } finally {
      setLoading(false)
    }
  }

  return (
    <div className="main-auth">
      <Row justify="center" align="middle">
        <Col sm={8} md={10} xs={20} className="col-auth">
          <Content>
            <div className="wrapper">
              <figure className="car-figure">
                <img src={Car} alt={SITE_NAME} data-cy="img-logo" />
              </figure>

              <div className="input-wrapper">
                <Input
                  size="large"
                  placeholder="E-mail"
                  value={model.email}
                  maxLength={32}
                  data-cy="input-email"
                  onChange={({target}) => setModel((prev) => ({ ...prev, email: target?.value }))}
                />
              </div>

              <div className="input-wrapper">
                <Input.Password
                  size="large"
                  placeholder="Senha"
                  value={model.password}
                  maxLength={16}
                  onChange={({target}) => setModel((prev) => ({ ...prev, password: target?.value }))}
                  data-cy="input-password"
                />
              </div>

              <div>
                <Button
                  block
                  size="large"
                  type="primary"
                  shape="round"
                  loading={loading}
                  disabled={!model.email || !model.password}
                  onClick={handleAuth}
                  data-cy="btn-auth"
                >
                  Efetuar Login
                </Button>
              </div>
            </div>
          </Content>
        </Col>
      </Row>
    </div>
  )
}

export default Auth
