import React from 'react'
import { Layout } from 'antd'

const Footer: React.FC = () => {

  return (
    <Layout.Footer className="main-footer-inner">
      Developer by Assunção Jr, utilizando
      &nbsp;<a href="http://reactjs.org/">React.JS</a> and <a href="https://ant.design/">Ant.Design</a>
    </Layout.Footer>
  )
}

export default Footer
