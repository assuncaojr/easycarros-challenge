import React, { useState } from 'react'
import Header from './header'
import Footer from './footer'
import HomePage from './Home'
import Auth from './Auth'
import { enUser } from 'enums'

const Main: React.FC = () => {
  const [token, setToken] = useState<string>(
    localStorage.getItem(enUser.TOKEN) || ''
  )

  if (!token) return <Auth callback={setToken} />

  return (
    <>
      <Header />
      <HomePage />
      <Footer />
    </>
  )
}

export default Main
