import React, { useState } from 'react'
import Container from 'components/Shared/Container'
import PlusOutlined from '@ant-design/icons/PlusOutlined'
import { Row, Col, Input, Button, Typography } from 'antd'
import { createVehicle } from '../actions'
import { handleError } from 'helpers/errors'
import './styles.less'

interface IProps {
  callback: Function
}

const CreateForm = ({ callback }: IProps) => {
  const [plate, setPlate] = useState<string>('')
  const [loading, setLoading] = useState<boolean>(false)

  const handleCreate = async () => {
    try {
      setLoading(true)

      await createVehicle(plate)
      await callback()
      setPlate('')
    } catch (err) {
      handleError(err.error || err)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Container>
      <div className="wapprer">
        <div style={{width: '100%'}}>
          <Row>
            <Col xs={24} sm={5}>
              <Typography.Title level={4}>Adicionar Veículo</Typography.Title>
            </Col>

            <Col xs={16} sm={15}>
              <Input
                size="large"
                placeholder="Placa"
                value={plate}
                maxLength={7}
                onChange={({target}) => setPlate(target?.value)}
              />
            </Col>

            <Col xs={8} sm={4}>
              <div style={{paddingLeft: 5}}>
                <Button
                  type="primary"
                  size="large"
                  block
                  loading={loading}
                  icon={<PlusOutlined />}
                  disabled={!(plate || '').match(/^[a-zA-Z0-9]{7}$/)}
                  onClick={handleCreate}
                  className="button-create"
                >
                  <span className="text">Adicionar</span>
                </Button>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </Container>
  )
}

export default CreateForm
