import { fetchApi } from 'helpers/api'

async function getVehicles() {
  try {
    const data = await fetchApi('vehicle', {
      method: 'GET'
    })

    return data
  } catch (err) {
    throw err
  }
}

async function createVehicle(plate: string) {
  try {
    const data = await fetchApi('vehicle', {
      method: 'POST',
      body: { plate }
    })

    return data
  } catch (err) {
    throw err
  }
}

async function deleteVehicle(id: string) {
  try {
    await fetchApi(`vehicle/${id}`, {
      method: 'DELETE'
    })
  } catch (err) {
    throw err
  }
}

export {
  getVehicles,
  createVehicle,
  deleteVehicle
}
