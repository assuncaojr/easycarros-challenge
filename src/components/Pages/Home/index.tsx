import React, { useEffect, useState, useCallback } from 'react'
import Container from 'components/Shared/Container'
import CreateForm from './form/create'
import { Table, Typography, Button } from 'antd'
import { deleteVehicle, getVehicles } from './actions'
import { handleError } from 'helpers/errors'
import { IVehicle } from 'interfaces'
import DeleteFilled from '@ant-design/icons/DeleteFilled'

interface ITableRow {
  key: string
  id: string
  plate: string
}

const Home = () => {
  const removeVehicle = async (id: string) => {
    try {
      await deleteVehicle(id)
      loadData()
    } catch (err) {
      handleError(err.error || err)
    }
  }

  const tableColumns = [
    {
      title: 'Veículos',
      dataIndex: 'plate',
      key: 'plate',
      render: (text: string) => <Typography.Text>{text}</Typography.Text>,
    },
    {
      title: 'Remover',
      key: 'action',
      render: (text: string, vehicle: IVehicle) => (
        <div className="has-text-right">
          <Button
            icon={<DeleteFilled />}
            onClick={() => removeVehicle(vehicle.id)}
            shape="round"
            danger
          >
            Remover
          </Button>
        </div>
      ),
    },
  ]

  const [tableRows, setTableRows] = useState<ITableRow[]>([])

  const loadData = useCallback(() => {
    getVehicles()
      .then((res) => {
        const { data: vehicles } = res
        const rows = vehicles.map((vehicle: IVehicle, index: number) => ({
          ...vehicle,
          key: index.toString()
        }))

        setTableRows(rows)
      })
      .catch(err => {
        handleError(err.error || err)
      })
  }, [])

  useEffect(() => loadData(), [loadData])

  return (
    <div className="main-home">
      <div className="create-form">
        <CreateForm callback={loadData} />
      </div>

      <Container>
        <Table
          dataSource={tableRows}
          columns={tableColumns}
          locale={{ emptyText: 'Não há veículos registrados no momento' }}
        />
      </Container>
    </div>
  )
}

export default Home
